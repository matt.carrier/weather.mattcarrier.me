#!/usr/bin/env bash
set -e

# bring up localstack and mockserver
docker-compose down && docker-compose up --build -d

until AWS_ACCESS_KEY_ID=fake AWS_SECRET_ACCESS_KEY=fake aws --endpoint-url=http://localhost:4575 sns list-topics
do
  echo 'waiting for localstack'
  sleep 1
done

docker run --rm --network local_default jwilder/dockerize dockerize \
  -wait tcp://docker.for.mac.localhost:8000

# create the open weather map API key secret
aws --endpoint-url http://localhost:4584 secretsmanager create-secret \
  --name OpenWeatherMapApiKey \
  --secret-string api-key

# create SNS topics
aws --endpoint-url http://localhost:4575 sns create-topic \
  --name weather-forecasts

aws --endpoint-url http://localhost:4575 sns create-topic \
  --name weather-rules

aws --endpoint-url http://localhost:4575 sns create-topic \
  --name weather-notifications

# create dynamo tables
aws --endpoint-url http://localhost:4569 dynamodb create-table \
  --table-name WeatherAlertRules \
  --attribute-definitions \
    AttributeName=Recipient,AttributeType=S \
    AttributeName=Id,AttributeType=S \
    AttributeName=Zip,AttributeType=S \
  --key-schema AttributeName=Recipient,KeyType=HASH AttributeName=Id,KeyType=RANGE \
  --billing-mode PAY_PER_REQUEST \
  --global-secondary-indexes \
    IndexName=WeatherAlertRulesByZip,\
Projection="{ProjectionType=ALL}",\
KeySchema=["{AttributeName=Zip,KeyType=HASH}","{AttributeName=Id,KeyType=RANGE}"]

aws --endpoint-url http://localhost:4569 dynamodb create-table \
  --table-name WeatherForecasts \
  --attribute-definitions AttributeName=Zip,AttributeType=S \
  --key-schema AttributeName=Zip,KeyType=HASH \
  --billing-mode PAY_PER_REQUEST

# create environment file for local invoke
RULE_TOPIC_ARN=$(aws --endpoint-url=http://localhost:4575 sns list-topics | jq .Topics[].TopicArn | grep rules)
FCST_TOPIC_ARN=$(aws --endpoint-url=http://localhost:4575 sns list-topics | jq .Topics[].TopicArn | grep forecasts)
NTFN_TOPIC_ARN=$(aws --endpoint-url=http://localhost:4575 sns list-topics | jq .Topics[].TopicArn | grep notifications)

rm -f env.json
echo "{" >> env.json
functions=( AddRule GetRules DeleteRule RulesAdded InitiateForecasts GetOWMForecast ForecastsAdded PerformRuleMatch RuleMatched )
for i in "${functions[@]}"
do
  echo "  \"$i\": {" >> env.json
  echo "    \"RULE_TOPIC_ARN\": $RULE_TOPIC_ARN," >> env.json
  echo "    \"FORECAST_TOPIC_ARN\": $FCST_TOPIC_ARN," >> env.json
  echo "    \"NOTIFICATION_TOPIC_ARN\": $NTFN_TOPIC_ARN" >> env.json

  if [[ "RuleMatched" == "$i" ]];
  then
    echo "  }" >> env.json
  else
    echo "  }," >> env.json
  fi
done
echo "}" >> env.json
