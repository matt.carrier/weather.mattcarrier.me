"""Utility module for interacting with AWS services.

This module encapsulates AWS service interaction.
"""

import boto3
import os


class AwsUtil:
    """Utilities for AWS service integration

    Attributes
    ----------
    owm_api_key : int
        the API key used with openweathermap.org
    dynamodb
        the dynamo db client
    ses
        the simple email service client
    """

    def __init__(self, sns=None, ssm=None, dynamodb=None, ses=None):
        self._sns = self._client_init(sns, 'sns', "http://docker.for.mac.localhost:4575/")
        self._ssm = self._client_init(ssm, 'secretsmanager', "http://docker.for.mac.localhost:4584/")
        self.ses = self._client_init(ses, 'ses', "http://docker.for.mac.localhost:4579/")
        self.dynamodb = self._client_init(dynamodb, 'dynamodb', "http://docker.for.mac.localhost:4569/")

        self._sns_rule_topic_arn = os.getenv('RULE_TOPIC_ARN')
        self._sns_forecast_topic_arn = os.getenv('FORECAST_TOPIC_ARN')
        self._sns_notification_topic_arn = os.getenv('NOTIFICATION_TOPIC_ARN')

        self.owm_api_key = self._ssm.get_secret_value(SecretId='OpenWeatherMapApiKey')['SecretString']

    @staticmethod
    def _client_init(client, boto_name, endpoint_url):
        """Initializes the boto client"""

        if None is not client:
            return client
        elif os.getenv("AWS_SAM_LOCAL"):
            return boto3.client(boto_name, endpoint_url=endpoint_url)
        else:
            return boto3.client(boto_name)

    def publish_rule_msg(self, msg):
        """Publishes a message to the Rule topic

        Parameters
        ----------
        msg : str
            the message to send to the topic
        """

        self._publish_sns_message(self._sns_rule_topic_arn, msg)

    def publish_forecast_msg(self, msg):
        """Publishes a message to the Forecast topic

        Parameters
        ----------
        msg : str
            the message to send to the topic
        """

        self._publish_sns_message(self._sns_forecast_topic_arn, msg)

    def publish_notification_msg(self, msg):
        """Publishes a message to the Notification topic

        Parameters
        ----------
        msg : str
            the message to send to the topic
        """

        self._publish_sns_message(self._sns_notification_topic_arn, msg)

    def _publish_sns_message(self, topic_arn, msg):
        self._sns.publish(TopicArn=topic_arn, Message=msg)


_aws = None


def aws_util(func):
    """Decorates a function by injecting an AwsUtil instance.

    Parameters
    ----------
    func : function
        the function to decorate

    Returns
    -------
    Function
        the decorated function
    """

    def get_util(*args, **kwargs):
        if kwargs["aws"] is None:
            global _aws
            _aws = AwsUtil() if _aws is None else _aws
            kwargs["aws"] = _aws

        return func(*args, **kwargs)

    return get_util
