"""Notification Lambda Handlers

This module contains all notification-related lambda handlers.
"""

from util.aws_util import aws_util

import pystache

_alert_subject = "Weather Alert ({{rule.name}})"
_alert_notification = """
Zip: {{rule.zip.zip}}
Country: {{rule.zip.country}}

{{#day_alerts}}
    Day: {{date}}
    Alert: {{amt}}mm of {{rule_unit}} is greater than or equal to {{rule_amt}}mm.

{{/day_alerts}}
"""


@aws_util
def when_rule_matched(event, context, aws=None):
    """Reacts to a rule matching forecasted weather conditions

    When a rule matches forecasted weather conditions we send an email notification (alert) to the rule recipient.

    Parameters
    ----------
    event : data-type
        the SNS event
    context : data-type
        the associated lambda context
    aws : AwsUtil
        utilities for AWS service interaction
    """

    message = event['Records'][0]['Sns']['Message']
    rule = message['rule']
    matched_days = message['matched_days']

    notification = pystache.render(
        _alert_notification,
        {
            'rule': rule,
            'day_alerts': list(map(lambda day: _transform_day(day, rule), matched_days))
        }
    )

    aws.ses.send_email(
        Source='weather@mattcarrier.me',
        Destination={'ToAddresses': [rule.recipient]},
        Message={
            'Subject': {'Data': pystache.render(_alert_subject, {'rule': rule})},
            'Body': {
                'Text': {'Data': notification}
            }
        },
        ReplyToAddresses=['weather@mattcarrier.me'],
        ReturnPath=['postmaster@mattcarrier.me']
    )


def _transform_day(day, rule):
    """Builds a day_alert from the given matched day and Rule"""

    matchers = {
        'precipitation': day.precipitation_volume,
        'rain': day.rain_volume,
        'snow': day.snow_volume
    }

    return {
        'date': day.date,
        'amt': matchers[rule.unit],
        'rule_unit': rule.unit,
        'rule_amt': rule.amt
    }
