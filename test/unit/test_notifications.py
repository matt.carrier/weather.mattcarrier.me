from datetime import datetime, timedelta
from domain.forecast import Zip, DayForecast
from domain.rule import Rule
from mockito import when, mock, unstub, verify
from mockito.matchers import any, captor
from pytest import fixture, mark
from util import aws_util as aws

import notifications

pytestmark = mark.unit

OWM_API_KEY = 'owm-api-key'
RULE_TOPIC_ARN = 'RULE_TOPIC_ARN'
FORECAST_TOPIC_ARN = 'FORECAST_TOPIC_ARN'
NOTIFICATION_TOPIC_ARN = 'NOTIFICATION_TOPIC_ARN'


@fixture
def ses():
    return mock()


@fixture
def aws_util(ses):
    when(aws.os).getenv(RULE_TOPIC_ARN).thenReturn(RULE_TOPIC_ARN)
    when(aws.os).getenv(FORECAST_TOPIC_ARN).thenReturn(FORECAST_TOPIC_ARN)
    when(aws.os).getenv(NOTIFICATION_TOPIC_ARN).thenReturn(NOTIFICATION_TOPIC_ARN)

    ssm = mock()
    when(ssm).get_secret_value(SecretId='OpenWeatherMapApiKey').thenReturn({'SecretString': OWM_API_KEY})
    return aws.AwsUtil(sns=mock(), ssm=ssm, dynamodb=mock(), ses=ses)


@fixture
def zip():
    return Zip('02818', 'US')


@fixture
def rule(zip):
    return Rule('East Greenwich, RI -- Snow (5)', 'recipient@email.com', zip, 'snow', 5)


@fixture
def matched_days():
    return [
        DayForecast(datetime.today(), 11, 5),
        DayForecast(datetime.today() + timedelta(days=1), 0, 7)
    ]


@fixture
def event(rule, matched_days):
    return {
        'Records': [
            {
                'Sns': {
                    'Message': {
                        'rule': rule,
                        'matched_days': matched_days
                    }
                }
            }
        ]
    }


def test_when_rule_matched(event, aws_util, ses, rule, matched_days):
    notifications.when_rule_matched(event, None, aws=aws_util)

    source_captor = captor(any())
    destination_captor = captor(any())
    message_captor = captor(any())
    reply_captor = captor(any())
    return_captor = captor(any())

    verify(ses).send_email(
        Source=source_captor,
        Destination=destination_captor,
        Message=message_captor,
        ReplyToAddresses=reply_captor,
        ReturnPath=return_captor
    )

    assert message_captor.value['Subject']['Data'] == 'Weather Alert ({})'.format(rule.name)
    assert rule.zip.zip in message_captor.value['Body']['Text']['Data']
    assert rule.zip.country in message_captor.value['Body']['Text']['Data']

    for day in matched_days:
        assert str(day.date) in message_captor.value['Body']['Text']['Data']
        assert "{}mm of {} is greater than or equal to {}mm.".format(day.snow_volume, rule.unit, rule.amt) in \
               message_captor.value['Body']['Text']['Data']

    assert source_captor.value == 'weather@mattcarrier.me'
    assert destination_captor.value['ToAddresses'][0] == rule.recipient
    assert reply_captor.value[0] == 'weather@mattcarrier.me'
    assert return_captor.value[0] == 'postmaster@mattcarrier.me'


def teardown_function():
    unstub()
