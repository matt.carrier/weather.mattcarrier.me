from datetime import datetime
from domain.forecast import Zip
from domain.rule import Rule
from mockito import when, mock, unstub, verify
from mockito.matchers import any, captor, eq
from pytest import fixture, mark
from util import aws_util as aws

import forecasts

pytestmark = mark.unit

OWM_API_KEY = 'owm-api-key'
RULE_TOPIC_ARN = 'RULE_TOPIC_ARN'
FORECAST_TOPIC_ARN = 'FORECAST_TOPIC_ARN'
NOTIFICATION_TOPIC_ARN = 'NOTIFICATION_TOPIC_ARN'


@fixture
def dynamodb():
    return mock()


@fixture
def aws_util(dynamodb):
    when(aws.os).getenv(RULE_TOPIC_ARN).thenReturn(RULE_TOPIC_ARN)
    when(aws.os).getenv(FORECAST_TOPIC_ARN).thenReturn(FORECAST_TOPIC_ARN)
    when(aws.os).getenv(NOTIFICATION_TOPIC_ARN).thenReturn(NOTIFICATION_TOPIC_ARN)

    ssm = mock()
    when(ssm).get_secret_value(SecretId='OpenWeatherMapApiKey').thenReturn({'SecretString': OWM_API_KEY})
    return aws.AwsUtil(sns=mock(), ssm=ssm, dynamodb=dynamodb, ses=mock())


@fixture
def zip():
    return Zip('02818', 'US')


@fixture
def rule(zip):
    return Rule('East Greenwich, RI -- Snow (5)', 'recipient@email.com', zip, 'snow', 5)


@fixture
def event(rule):
    return {
        'Records': [
            {
                'dynamodb': {
                    'NewImage': {
                        'Name': {
                            'S': rule.name
                        },
                        'Recipient': {
                            'S': rule.recipient
                        },
                        'Zip': {
                            'S': rule.zip.to_json()
                        },
                        'Unit': {
                            'S': rule.unit
                        },
                        'Amount': {
                            'N': rule.amt
                        },
                        'Id': {
                            'S': rule.id
                        }
                    }
                }
            }
        ]
    }


def test_when_rules_added(event, aws_util, dynamodb, rule):
    forecasts.when_rules_added(event, None, aws=aws_util)

    item_captor = captor(any())

    verify(dynamodb).put_item(
        TableName=eq('WeatherForecasts'),
        Item=item_captor,
        ConditionExpression=eq('attribute_not_exists(Zip)')
    )

    assert item_captor.value['Zip']['S'] == rule.zip.to_json()
    assert item_captor.value['Days']['L'] == []
    assert item_captor.value['DateTime']['N'] <= datetime.now()


def teardown_function():
    unstub()
