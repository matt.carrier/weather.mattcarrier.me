from pytest import fixture, mark
from domain.forecast import Zip
from domain.rule import Rule

import json
import re


pytestmark = mark.unit


@fixture
def zip():
    return Zip('02818', 'US')


@fixture
def rule(zip):
    return Rule('East Greenwich, RI -- Snow (5)', 'recipient@email.com', zip, 'snow', 5)


def test_rule_creation(rule, zip):
    assert 'East Greenwich, RI -- Snow (5)' == rule.name
    assert 'recipient@email.com' == rule.recipient
    assert zip == rule.zip
    assert 'snow' == rule.unit
    assert 5 == rule.amt
    assert re.compile('^[0-9A-Za-z]*$').match(rule.id) is not None


def test_rule_creation_with_id(rule):
    rule_with_id = Rule(rule.name, rule.recipient, rule.zip, rule.unit, rule.amt, 'id')

    assert rule.name == rule_with_id.name
    assert rule.recipient == rule_with_id.recipient
    assert rule.zip == rule_with_id.zip
    assert rule.unit == rule_with_id.unit
    assert rule.amt == rule_with_id.amt
    assert 'id' == rule_with_id.id


def test_rule_json(rule):
    rule_json = rule.to_json()
    deserialized = json.loads(rule_json)

    assert rule.id == deserialized['id']
    assert rule.name == deserialized['name']
    assert rule.recipient == deserialized['recipient']
    assert rule.zip.zip == deserialized['zip']['zip']
    assert rule.zip.country == deserialized['zip']['country']
    assert rule.unit == deserialized['unit']
    assert rule.amt == deserialized['amt']

    rule_deserialized = Rule.from_json(rule_json)
    assert rule == rule_deserialized


def test_rule_dynamodb_item(rule):
    rule_item = rule.to_dynamodb_item()

    assert rule.id == rule_item['Id']['S']
    assert rule.name == rule_item['Name']['S']
    assert rule.recipient == rule_item['Recipient']['S']
    assert rule.zip == Zip.from_json(rule_item['Zip']['S'])
    assert rule.unit == rule_item['Unit']['S']
    assert rule.amt == int(rule_item['Amount']['N'])

    rule_deserialized = Rule.from_dynamodb_item(rule_item)
    assert rule == rule_deserialized


def test_rule_from_body(rule):
    body = {
        'name': rule.name,
        'zip': rule.zip.zip,
        'country': rule.zip.country,
        'unit': rule.unit,
        'amt': rule.amt
    }

    outcome = Rule.from_body(rule.recipient, json.dumps(body))

    assert outcome[0]
    assert rule == outcome[1]


def test_rule_from_body_name_validation_required(rule):
    body = {
        'zip': rule.zip.zip,
        'country': rule.zip.country,
        'unit': rule.unit,
        'amt': rule.amt
    }

    _assert_error(Rule.from_body(rule.recipient, json.dumps(body)), "name", "required field")


def test_rule_from_body_name_validation_empty(rule):
    body = {
        'name': '',
        'zip': rule.zip.zip,
        'country': rule.zip.country,
        'unit': rule.unit,
        'amt': rule.amt
    }

    _assert_error(Rule.from_body(rule.recipient, json.dumps(body)), "name", "empty values not allowed")


def test_rule_from_body_name_validation_string(rule):
    body = {
        'name': 3,
        'zip': rule.zip.zip,
        'country': rule.zip.country,
        'unit': rule.unit,
        'amt': rule.amt
    }

    _assert_error(Rule.from_body(rule.recipient, json.dumps(body)), "name", "must be of string type")


def test_rule_from_body_name_validation_max(rule):
    body = {
        'name': "x" * 51,
        'zip': rule.zip.zip,
        'country': rule.zip.country,
        'unit': rule.unit,
        'amt': rule.amt
    }

    _assert_error(Rule.from_body(rule.recipient, json.dumps(body)), "name", "max length is 50")


def test_rule_from_body_zip_validation_required(rule):
    body = {
        'name': rule.name,
        'country': rule.zip.country,
        'unit': rule.unit,
        'amt': rule.amt
    }

    _assert_error(Rule.from_body(rule.recipient, json.dumps(body)), "zip", "required field")


def test_rule_from_body_zip_validation_string(rule):
    body = {
        'name': rule.name,
        'zip': 11111,
        'country': rule.zip.country,
        'unit': rule.unit,
        'amt': rule.amt
    }

    _assert_error(Rule.from_body(rule.recipient, json.dumps(body)), "zip", "must be of string type")


def test_rule_from_body_zip_validation_regex(rule):
    body = {
        'name': rule.name,
        'zip': "O2818",
        'country': rule.zip.country,
        'unit': rule.unit,
        'amt': rule.amt
    }

    _assert_error(Rule.from_body(rule.recipient, json.dumps(body)),
                  "zip",
                  "value does not match regex '^[0-9]{5}$'")


def test_rule_from_body_country_validation_required(rule):
    body = {
        'name': rule.name,
        'zip': rule.zip.zip,
        'unit': rule.unit,
        'amt': rule.amt
    }

    _assert_error(Rule.from_body(rule.recipient, json.dumps(body)), "country", "required field")


def test_rule_from_body_country_validation_string(rule):
    body = {
        'name': rule.name,
        'zip': rule.zip.zip,
        'country': 11,
        'unit': rule.unit,
        'amt': rule.amt
    }

    _assert_error(Rule.from_body(rule.recipient, json.dumps(body)), "country", "must be of string type")


def test_rule_from_body_country_validation_regex(rule):
    body = {
        'name': rule.name,
        'zip': rule.zip.zip,
        'country': "01",
        'unit': rule.unit,
        'amt': rule.amt
    }

    _assert_error(Rule.from_body(rule.recipient, json.dumps(body)),
                  "country",
                  "value does not match regex '^[A-Za-z]{2}$'")


def test_rule_from_body_unit_validation_required(rule):
    body = {
        'name': rule.name,
        'zip': rule.zip.zip,
        'country': rule.zip.country,
        'amt': rule.amt
    }

    _assert_error(Rule.from_body(rule.recipient, json.dumps(body)), "unit", "required field")


def test_rule_from_body_unit_validation_string(rule):
    body = {
        'name': rule.name,
        'zip': rule.zip.zip,
        'country': rule.zip.country,
        'unit': 1,
        'amt': rule.amt
    }

    _assert_error(Rule.from_body(rule.recipient, json.dumps(body)), "unit", "must be of string type")


def test_rule_from_body_unit_validation_allowed(rule):
    body = {
        'name': rule.name,
        'zip': rule.zip.zip,
        'country': rule.zip.country,
        'unit': 'precip',
        'amt': rule.amt
    }

    _assert_error(Rule.from_body(rule.recipient, json.dumps(body)), "unit", "unallowed value precip")


def test_rule_from_body_amt_validation_required(rule):
    body = {
        'name': rule.name,
        'zip': rule.zip.zip,
        'country': rule.zip.country,
        'unit': rule.unit
    }

    _assert_error(Rule.from_body(rule.recipient, json.dumps(body)), "amt", "required field")


def test_rule_from_body_amt_validation_integer(rule):
    body = {
        'name': rule.name,
        'zip': rule.zip.zip,
        'country': rule.zip.country,
        'unit': rule.unit,
        'amt': "3"
    }

    _assert_error(Rule.from_body(rule.recipient, json.dumps(body)), "amt", "must be of integer type")


def test_rule_from_body_amt_validation_min(rule):
    body = {
        'name': rule.name,
        'zip': rule.zip.zip,
        'country': rule.zip.country,
        'unit': rule.unit,
        'amt': -1
    }

    _assert_error(Rule.from_body(rule.recipient, json.dumps(body)), "amt", "min value is 0")


def test_rule_from_body_amt_validation_max(rule):
    body = {
        'name': rule.name,
        'zip': rule.zip.zip,
        'country': rule.zip.country,
        'unit': rule.unit,
        'amt': 101
    }

    _assert_error(Rule.from_body(rule.recipient, json.dumps(body)), "amt", "max value is 100")


def _assert_error(outcome, field, msg):
    assert not outcome[0]

    errors = json.loads(outcome[1])
    assert 1 == len(errors)
    assert 1 == len(errors[field])
    assert msg == errors[field][0]
