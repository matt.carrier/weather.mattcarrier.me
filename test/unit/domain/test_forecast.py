from pytest import fixture, mark
from domain.forecast import DayForecast, Forecast, Zip
from domain.json_encoder import WeatherJsonEncoder

import datetime
import json
import time

pytestmark = mark.unit


@fixture
def zip():
    return Zip('02818', 'US')


@fixture
def days():
    return [DayForecast(datetime.datetime.now(), 1, 2)]


@fixture
def forecast(zip, days):
    return Forecast(zip, days=days)


def test_zip_creation(zip):
    assert '02818' == zip.zip
    assert 'US' == zip.country
    assert zip == Zip('02818', 'US')


def test_dayforecast_creation():
    date = datetime.datetime.now()
    df = DayForecast(date, 1, 2)

    assert date == df.date
    assert 1 == df.rain_volume
    assert 2 == df.snow_volume
    assert 3 == df.precipitation_volume
    assert df == DayForecast(date, 1, 2)


def test_forecast_creation(forecast, zip, days):
    assert zip == forecast.zip
    assert days == forecast.days
    assert days[0] == forecast.days[0]
    assert datetime.datetime.now() >= forecast.dt
    assert forecast == Forecast(zip, forecast.dt, days)


def test_forecast_creation_with_dt(zip):
    dt = time.time()
    forecast = Forecast(zip, dt)

    assert [] == forecast.days
    assert zip == forecast.zip
    assert dt == forecast.dt


def test_zip_json(zip):
    zip_json = zip.to_json()
    deserialized = json.loads(zip_json)

    assert zip.zip == deserialized['zip']
    assert zip.country == deserialized['country']

    zip_deserialized = Zip.from_json(zip_json)

    assert zip == zip_deserialized


def test_forecast_json(forecast):
    forecast_json = forecast.to_json()
    deserialized = json.loads(forecast_json)

    assert forecast.zip.zip == deserialized['zip']['zip']
    assert forecast.zip.country == deserialized['zip']['country']
    assert forecast.dt == datetime.datetime.fromtimestamp(deserialized['dt'])
    assert forecast.days[0].date == datetime.datetime.fromtimestamp(deserialized['days'][0]['date'])
    assert forecast.days[0].rain_volume == deserialized['days'][0]['rain_volume']
    assert forecast.days[0].snow_volume == deserialized['days'][0]['snow_volume']
    assert forecast.days[0].precipitation_volume == deserialized['days'][0]['precipitation_volume']

    forecast_deserialized = Forecast.from_json(forecast_json)
    assert forecast == forecast_deserialized


def test_forecast_to_dynamodb_item(forecast):
    forecast_item = forecast.to_dynamodb_item()

    assert forecast.zip == Zip.from_json(forecast_item['Zip']['S'])
    assert forecast.dt == datetime.datetime.fromtimestamp(forecast_item['DateTime']['N'])
    assert json.dumps(forecast.days, cls=WeatherJsonEncoder) == forecast_item['Days']['L']

    forecast_deserialized = Forecast.from_dynamodb_item(forecast_item)
    assert forecast == forecast_deserialized


def test_get_owm_forecast(zip):
    forecast = Forecast.from_owm_forecast(zip, open("data/forecast.json", "r").read())

    assert zip == forecast.zip
    assert datetime.datetime.now() >= forecast.dt

    assert DayForecast(datetime.datetime.strptime('2019-05-06', '%Y-%m-%d').date(), 0, 0) == forecast.days[0]
    assert DayForecast(datetime.datetime.strptime('2019-05-07', '%Y-%m-%d').date(), 0, 0) == forecast.days[1]
    assert DayForecast(datetime.datetime.strptime('2019-05-08', '%Y-%m-%d').date(), 1, 0) == forecast.days[2]
    assert DayForecast(datetime.datetime.strptime('2019-05-09', '%Y-%m-%d').date(), 2, 0) == forecast.days[3]
    assert DayForecast(datetime.datetime.strptime('2019-05-10', '%Y-%m-%d').date(), 44, 0) == forecast.days[4]
    assert DayForecast(datetime.datetime.strptime('2019-05-11', '%Y-%m-%d').date(), 13, 0) == forecast.days[5]
