from mockito import when, mock, unstub, verify
from pytest import fixture, mark
from util import aws_util as aws

pytestmark = mark.unit

OWM_API_KEY = 'owm-api-key'
RULE_TOPIC_ARN = 'RULE_TOPIC_ARN'
FORECAST_TOPIC_ARN = 'FORECAST_TOPIC_ARN'
NOTIFICATION_TOPIC_ARN = 'NOTIFICATION_TOPIC_ARN'


@fixture
def sns():
    return mock()


@fixture
def aws_util(sns):
    when(aws.os).getenv(RULE_TOPIC_ARN).thenReturn(RULE_TOPIC_ARN)
    when(aws.os).getenv(FORECAST_TOPIC_ARN).thenReturn(FORECAST_TOPIC_ARN)
    when(aws.os).getenv(NOTIFICATION_TOPIC_ARN).thenReturn(NOTIFICATION_TOPIC_ARN)

    ssm = mock()
    when(ssm).get_secret_value(SecretId='OpenWeatherMapApiKey').thenReturn({'SecretString': OWM_API_KEY})
    return aws.AwsUtil(sns=sns, ssm=ssm, dynamodb=mock(), ses=mock())


def teardown_function():
    unstub()


def test_owm_api_key(aws_util):
    assert OWM_API_KEY == aws_util.owm_api_key


def test_publish_rule_msg(sns, aws_util):
    aws_util.publish_rule_msg("message")
    verify(sns).publish(TopicArn=RULE_TOPIC_ARN, Message="message")


def test_publish_forecast_msg(sns, aws_util):
    aws_util.publish_forecast_msg("message")
    verify(sns).publish(TopicArn=FORECAST_TOPIC_ARN, Message="message")


def test_publish_notification_msg(sns, aws_util):
    aws_util.publish_notification_msg("message")
    verify(sns).publish(TopicArn=NOTIFICATION_TOPIC_ARN, Message="message")
