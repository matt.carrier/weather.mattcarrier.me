"""Forecast Lambda Handlers

This module contains all forecast-related lambda handlers.
"""

from domain.forecast import Forecast, Zip
from domain.rule import Rule
from util.aws_util import aws_util

import http.client
import os


class _OwmConfig:
    """Configuration for OWM"""

    def __init__(self):
        if os.getenv("AWS_SAM_LOCAL"):
            self._use_https = False
            self._hostname = 'docker.for.mac.localhost:8000'
        else:
            self._use_https = True
            self._hostname = 'api.openweathermap.org'

    def get_connection(self):
        """Creates a new connection to OWM"""

        if self._use_https:
            return http.client.HTTPSConnection(self._hostname)
        else:
            return http.client.HTTPConnection(self._hostname)


_owm_config = _OwmConfig()


@aws_util
def when_rules_added(event, context, aws):
    """Reacts to new rules added

    When a new rule gets added we check to make sure that a Forecast exists for the Zip associated to the Rule. If one
    does not exist than we create an empty Forecast so that it gets picked up on the next Forecast run.

    Parameters
    ----------
    event : data-type
        the dynamodb stream event for rule insertion
    context : data-type
        the associated lambda context
    aws : AwsUtil
        utilities for AWS service interaction
    """

    map(lambda record: _insert_initial(Rule.from_dynamodb_item(record['dynamodb']['NewImage']), aws), event['Records'])


def _insert_initial(rule, aws):
    """Adds an initial empty Forecast for a rule if no Forecast currently exists for associated Zip"""

    aws.dynamodb.put_item(
        TableName='WeatherForecasts',
        Item=Forecast(Zip(rule.zip.zip, rule.zip.country)).to_dynamodb_item(),
        ConditionExpression="attribute_not_exists(Zip)"
    )


@aws_util
def initiate(event, context, aws):
    """Initiates new Forecasts

    Parameters
    ----------
    event : data-type
        the cloud watch scheduled event
    context : data-type
        the associated lambda context
    aws : AwsUtil
        utilities for AWS service interaction
    """

    last_evaluated_key = None
    while True:
        # you can't pass in None for ExclusiveStartKey ... sadpanda
        if last_evaluated_key is None:
            resp = aws.dynamodb.scan(TableName="WeatherForecasts")
        else:
            resp = aws.dynamodb.scan(TableName="WeatherForecasts", ExclusiveStartKey=last_evaluated_key)

        for item in resp['Items']:
            aws.publish_forecast_msg(item[Zip]['S'])

        if "LastEvaluatedKey" not in resp:
            break

        last_evaluated_key = resp['LastEvaluatedKey']


@aws_util
def owm_get(event, context, aws):
    """Retrieves a new 5 day forecast for a given Zip from openweathermap.org

    Parameters
    ----------
    event : data-type
        the SNS event
    context : data-type
        the associated lambda context
    aws : AwsUtil
        utilities for AWS service interaction
    """

    zip = Zip.from_json(event['Records'][0]['Sns']['Message'])
    forecast = _owm_config.get_connection().request(
        'GET',
        '/data/2.5/forecast?zip={}&country={}&appid={}'.format(zip.zip, zip.country, aws.owm_api_key)
    )

    aws.dynamodb.put_item(
        TableName="WeatherForecasts",
        Item=Forecast.from_owm_forecast(zip, forecast).to_dynamodb_item()
    )
