"""Rule Lambda Handlers

This module contains all rule-related lambda handlers.
"""

from domain.forecast import Forecast
from domain.rule import Rule
from util.aws_util import aws_util

import json


@aws_util
def add(event, context, aws):
    """RESTful Rule POST

    Parameters
    ----------
    event : data-type
        the API Gateway POST event
    context : data-type
        the associated lambda context
    aws : AwsUtil
        utilities for AWS service interaction
    """

    outcome = Rule.from_body('mcarrieruri@gmail.com', event['body'])
    if not outcome[0]:
        return {
            "statusCode": 200,
            "body": outcome[1]
        }

    rule = outcome[1]
    aws.dynamodb.put_item(
        TableName='WeatherAlertRules',
        Item=rule.to_dynamodb_item()
    )

    return {
        "statusCode": 200,
        "body": rule.to_json()
    }


@aws_util
def get(event, context, aws):
    """RESTful Rule GET

    Parameters
    ----------
    event : data-type
        the API Gateway GET event
    context : data-type
        the associated lambda context
    aws : AwsUtil
        utilities for AWS service interaction
    """

    return list(map(lambda rule: {rule.to_json()}, aws.dynamodb.query(
        ExpressionAttributeValues={
            ':recipient': {'S': 'mcarrieruri@gmail.com'},
        },
        KeyConditionExpression='Recipient = :recipient',
        TableName='WeatherAlertRules'
    )['Items']))


@aws_util
def delete(event, context, aws):
    """RESTful Rule DELETE

    Parameters
    ----------
    event : data-type
        the API Gateway DELETE event
    context : data-type
        the associated lambda context
    aws : AwsUtil
        utilities for AWS service interaction
    """

    aws.dynamodb.delete_item(
        Key={
            'Id': {'S': event['path_parameters']['id']},
            'Recipient': {'S': 'mcarrieruri@gmail.com'}
        },
        TableName='WeatherAlertRules'
    )

    return {"statusCode": 204}


@aws_util
def when_forecasts_added(event, context, aws):
    """Reacts to when new Forecasts have been added

    When a new Forecast has been added (and is not empty) we need to check if any rules match the new forecasted data.

    Parameters
    ----------
    event : data-type
        the Dynamo DB stream event
    context : data-type
        the associated lambda context
    aws : AwsUtil
        utilities for AWS service interaction
    """

    map(
        lambda record: _when_forecast_added(Forecast.from_dynamodb_item(record['dynamodb']['NewImage']), aws),
        event['Records']
    )


def _when_forecast_added(forecast, aws):
    """If the Forecast is not empty then kick off the rule matching"""

    if forecast.days:
        aws.publish_rule_msg(forecast.to_json())


@aws_util
def rule_match(event, context, aws):
    """Finds any rules that match the weather conditions of the new Forecast

    Parameters
    ----------
    event : data-type
        the SNS event
    context : data-type
        the associated lambda context
    aws : AwsUtil
        utilities for AWS service interaction
    """

    forecast = Forecast.from_json(event['Records'][0]['Sns']['Message'])

    last_evaluated_key = None
    while True:
        resp = aws.dynamodb.query(
            TableName='WeatherAlertRules',
            IndexName='WeatherAlertRulesByZip',
            ExclusiveStartKey=last_evaluated_key,
            ExpressionAttributeValues={':zip': {'S': forecast.zip.to_json()}},
            KeyConditionExpression='Zip = :zip',
        )
        for item in resp['Items']:
            _perform_rule_match(Rule.from_dynamodb_item(item), forecast, aws)

        last_evaluated_key = resp['LastEvaluatedKey']
        if last_evaluated_key is None:
            break


def _perform_rule_match(rule, forecast, aws):
    """Checks if the Rule matches the Forecast data and if it does it kicks off the notification"""

    matchers = {
        'precipitation': lambda day: day.precipitation_volume > rule.amt,
        'rain': lambda day: day.rain_volume > rule.amt,
        'snow': lambda day: day.snow_volume > rule.amt
    }

    matched_days = filter(matchers[rule.unit], forecast.days)
    if matched_days:
        aws.publish_notification_msg(json.dumps({
            'rule': rule,
            'matched_days': matched_days
        }))
