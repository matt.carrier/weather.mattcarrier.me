import json


class WeatherJsonEncoder(json.JSONEncoder):
    """JSONEncoder for Weather Forecasts and Rules"""

    def default(self, o):
        try:
            return o.to_json_dict()
        except NotImplementedError:
            return json.JSONEncoder.default(self, o)
