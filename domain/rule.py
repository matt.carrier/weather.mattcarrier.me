"""Rule Domain Models

This module contains all rule-related domain models.
"""

from cerberus import Validator
from hashlib import md5
from domain.forecast import Zip

import json

# rule attribute validation schema
RULE_SCHEMA = {
    'name':    {'required': True, 'empty': False, 'type': 'string', 'maxlength': 50},
    'zip':     {'required': True, 'type': 'string', 'regex': '^[0-9]{5}$'},
    'country': {'required': True, 'type': 'string', 'regex': '^[A-Za-z]{2}$'},
    'unit':    {'required': True, 'type': 'string', 'allowed': ['precipitation', 'rain', 'snow']},
    'amt':     {'required': True, 'type': 'integer', 'min': 0, 'max': 100}
}


class Rule:
    """
    A Rule defines what weather conditions trigger an alert and to what recipient.

    ...

    Attributes
    ----------
    name : str
        the display name
    recipient : str
        the email address of the recipient
    zip : Zip
        the zipcode and country associated with the forecast
    unit : str
        the weather trigger (for example snow or rain)
    amt : int
        the amount of the weather unit that causes an alert (in mm)
    id: str
        the identifier
    """

    def __init__(self, name, recipient, zip, unit, amt, id=None):
        """
        Parameters
        ----------
        name : str
            the display name
        recipient : str
            the email address of the recipient
        zip : Zip
            the zipcode and country associated with the forecast
        unit : str
            the weather trigger (for example snow or rain)
        amt : int
            the amount of the weather unit that causes an alert (in mm)
        id: str
            the identifier
        """

        self.name = name
        self.recipient = recipient
        self.zip = zip
        self.unit = unit
        self.amt = amt
        self.id = id or md5('{}-{}-{}-{}'.format(zip.zip, zip.country, unit, amt).encode()).hexdigest()

    @staticmethod
    def from_body(user_email, event_body):
        """Constructs a Rule from a POST body.

        Parameters
        ----------
        user_email : str
            the email of the user to create the alert for
        event_body : str
            the json body that contains the rule to be created

        Returns
        -------
        Tuple
            tuple containing whether the Rule has passed validation and the associated outcome (validation errors or the
            generated rule)
        """

        rule_dict = json.loads(event_body)
        validator = Validator(RULE_SCHEMA)
        if not validator.validate(rule_dict):
            return False, json.dumps(validator.errors)

        return True, Rule(
            rule_dict['name'],
            user_email,
            Zip(rule_dict['zip'], rule_dict['country']),
            rule_dict['unit'],
            rule_dict['amt']
        )

    @staticmethod
    def from_dynamodb_item(item):
        """Constructs a Rule from a dynamodb item.

        Parameters
        ----------
        item : data-type
            the dynamodb item

        Returns
        -------
        Rule
            the constructed Rule
        """

        return Rule(
            item['Name']['S'],
            item['Recipient']['S'],
            Zip.from_json(item['Zip']['S']),
            item['Unit']['S'],
            int(item['Amount']['N']),
            item['Id']['S']
        )

    @staticmethod
    def from_json(rule_json):
        """Constructs a Rule from json.

        Parameters
        ----------
        rule_json : str
            the rule as json

        Returns
        -------
        Rule
            the deserialized Rule
        """

        rule = json.loads(rule_json)
        return Rule(
            rule['name'],
            rule['recipient'],
            Zip(rule['zip']['zip'], rule['zip']['country']),
            rule['unit'],
            rule['amt'],
            rule['id']
        )

    def to_dynamodb_item(self):
        """Constructs a dynamodb item for this Rule.

        Returns
        -------
        dict
            the dynamodb item for this Rule
        """

        return {
            'Id':        {'S': self.id},
            'Recipient': {'S': self.recipient},
            'Name':      {'S': self.name},
            'Zip':       {'S': self.zip.to_json()},
            'Unit':      {'S': self.unit},
            'Amount':    {'N': str(self.amt)}
        }

    def to_json(self):
        """Serializes this Rule to json.

        Returns
        -------
        str
            this Rule serialized as json
        """

        return json.dumps(self.to_json_dict())

    def to_json_dict(self):
        """Returns a dictionary for json encoding.

        Returns
        -------
        dict
            this Rule as a dictionary
        """

        return {
            'id':           self.id,
            'recipient':    self.recipient,
            'name':         self.name,
            'zip':          self.zip.to_json_dict(),
            'unit':         self.unit,
            'amt':          self.amt
        }

    def __eq__(self, other):
        if not isinstance(other, Rule):
            return False

        return self.id == other.id and \
            self.recipient == other.recipient and \
            self.name == other.name and \
            self.zip == other.zip and \
            self.unit == other.unit and \
            self.amt == other.amt

    def __hash__(self):
        return hash((self.id, self.recipient, self.name, self.zip, self.unit, self.amt))
