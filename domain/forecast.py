"""Forecast Domain Models

This module contains all forecast-related domain models.
"""

from datetime import datetime
from domain.json_encoder import WeatherJsonEncoder

import json


class Forecast:
    """
    A Forecast holds information about future weather at a location
    
    ...
    
    Attributes
    ----------
    zip : Zip
        the zipcode and country associated with the forecast
    dt : int
        unix timestamp of the when the forecast was retrieved (default value of now)
    days: list of DayForecast
        the forecast data for each of the days in the forecast (default no days)
    """

    def __init__(self, zip, dt=datetime.now(), days=None):
        """
        Parameters
        ----------
        zip : Zip
            the zipcode and country associated with the forecast
        dt : int
            unix timestamp of the when the forecast was retrieved (default value of now)
        days: list of DayForecast
            the forecast data for each of the days in the forecast (default no days)
        """

        self.zip = zip
        self.dt = dt
        self.days = days or []

    @staticmethod
    def from_dynamodb_item(item):
        """Constructs a Forecast from a dynamodb item.

        Parameters
        ----------
        item : data-type
            the dynamodb item

        Returns
        -------
        Forecast
            the constructed Forecast
        """

        return Forecast(
            Zip.from_json(item['Zip']['S']),
            datetime.fromtimestamp(item['DateTime']['N']),
            list(map(
                lambda day: DayForecast(datetime.fromtimestamp(day['date']), day['rain_volume'], day['snow_volume']),
                json.loads(item['Days']['L'])
            ))
        )

    @staticmethod
    def from_json(forecast_json):
        """Constructs a Forecast from json.

        Parameters
        ----------
        forecast_json : str
            the forecast as json

        Returns
        -------
        Forecast
            the deserialized Forecast
        """

        forecast_dict = json.loads(forecast_json)
        return Forecast(
            Zip(forecast_dict['zip']['zip'], forecast_dict['zip']['country']),
            datetime.fromtimestamp(forecast_dict['dt']),
            list(map(
                lambda day: DayForecast(datetime.fromtimestamp(day['date']), day['rain_volume'], day['snow_volume']),
                forecast_dict['days']
            ))
        )

    @staticmethod
    def from_owm_forecast(zip, owm_forecast):
        """Constructs a Forecast from an openweathermap.org forecast.

        Parameters
        ----------
        zip : Zip
            the zipcode and country associated with the forecast
        owm_forecast : str
            the openweathermap.org forecast as json

        Returns
        -------
        Forecast
            the constructed Forecast
        """

        dict_forecast = json.loads(owm_forecast)

        days = []
        current = {
            'rain': 0.0,
            'snow': 0.0
        }
        for partial in dict_forecast['list']:
            date = datetime.utcfromtimestamp(partial['dt']).date()
            if 'date' not in current:
                current['date'] = date

            # if we have reached a new day then add the previous day to the forecast and clear the current data
            if current['date'] != date:
                days.append(DayForecast(current['date'], int(current['rain']), int(current['snow'])))

                current['date'] = date
                current['rain'] = 0.0
                current['snow'] = 0.0

            if 'rain' in partial and '3h' in partial['rain']:
                current['rain'] = current['rain'] + partial['rain']['3h']

            if 'snow' in partial and '3h' in partial['snow']:
                current['snow'] = current['snow'] + partial['snow']['3h']

        days.append(DayForecast(current['date'], int(current['rain']), int(current['snow'])))
        return Forecast(zip, days=days)

    def to_dynamodb_item(self):
        """Constructs a dynamodb item for this Forecast.

        Returns
        -------
        dict
            the dynamodb item for this Forecast
        """

        return {
            'Zip':      {'S': self.zip.to_json()},
            'DateTime': {'N': self.dt.timestamp()},
            'Days':     {'L': json.dumps(self.days, cls=WeatherJsonEncoder)}
        }

    def to_json(self):
        """Serializes this Forecast to json.

        Returns
        -------
        str
            this Forecast serialized as json
        """

        return json.dumps({
            'zip':  self.zip.to_json_dict(),
            'dt':   self.dt.timestamp(),
            'days': self.days
        }, cls=WeatherJsonEncoder)

    def __eq__(self, other):
        if not isinstance(other, Forecast):
            return False

        return self.zip == other.zip and self.days == other.days and self.dt == other.dt

    def __hash__(self):
        return hash((self.zip, self.days, self.dt))


class Zip:
    """
    A Zip holds zipcode and country information

    ...

    Attributes
    ----------
    zip : str
        the zipcode
    country : str
        the country

    Methods
    -------
    from_json(forecast_json)
        Constructs a Zip from json
    to_json()
        Serializes this Zip into json
    """

    def __init__(self, zip, country):
        """
        Parameters
        ----------
        zip : str
            the zipcode
        country : str
            the country
        """

        self.zip = zip
        self.country = country

    @staticmethod
    def from_json(json_zip):
        """Constructs a Zip from json.

        Parameters
        ----------
        json_zip : str
            the Zip as json

        Returns
        -------
        Zip
            the deserialized Zip
        """

        dict_zip = json.loads(json_zip)
        return Zip(dict_zip['zip'], dict_zip['country'])

    def to_json(self):
        """Serializes this Zip to json.

        Returns
        -------
        str
            this Zip serialized as json
        """

        return json.dumps(self.to_json_dict())

    def to_json_dict(self):
        """Returns a dictionary for json encoding.

        Returns
        -------
        dict
            this Zip as a dictionary
        """

        return {
            'zip':     self.zip,
            'country': self.country
        }

    def __eq__(self, other):
        if not isinstance(other, Zip):
            return False

        return self.zip == other.zip and self.country == other.country

    def __hash__(self):
        return hash((self.zip, self.country))


class DayForecast:
    """
    A DayForecast holds information about future weather at a location for a day.

    ...

    Attributes
    ----------
    date : date
        the date of the forecasted day
    rain_volume : float
        amount of rain in mm for the given day
    snow_volume: float
        amount of snow in mm for the given day
    precipitation_volume: float
        amount of precipitation (rain and/or snow) in mm for the given day
    """

    def __init__(self, date, rain_volume, snow_volume):
        """
        Parameters
        ----------
        date : date
            the date of the forecasted day
        rain_volume : float
            amount of rain in mm for the given day
        snow_volume: float
            amount of snow in mm for the given day
        """

        self.date = date
        self.rain_volume = rain_volume
        self.snow_volume = snow_volume
        self.precipitation_volume = rain_volume + snow_volume

    def to_json_dict(self):
        """Returns a dictionary for json encoding.

        Returns
        -------
        dict
            this DayForecast as a dictionary
        """

        return {
            'date':     self.date.timestamp(),
            'rain_volume': self.rain_volume,
            'snow_volume': self.snow_volume,
            'precipitation_volume': self.precipitation_volume
        }

    def __eq__(self, other):
        if not isinstance(other, DayForecast):
            return False

        return self.date == other.date and \
            self.rain_volume == other.rain_volume and \
            self.snow_volume == other.snow_volume

    def __hash__(self):
        return hash((self.date, self.rain_volume, self.snow_volume))
